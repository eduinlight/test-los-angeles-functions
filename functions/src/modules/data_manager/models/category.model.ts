import Department from './department.model'

export default interface Category{
  id?: string;
  name: string;
  departmentRef: Department['id'] | Department
}
