import Department from './department.model'
import Category from './category.model'

export default interface Product{
  id?: string;
  name: string;
  cost: number;
  departmentRef: Department['id'] | Department
  categoryRef: Category['id'] | Category
}
