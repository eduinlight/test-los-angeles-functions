import Repository from '../core/repository'
import Product from '../models/product.model'

class ProductRepositoryClass extends Repository<Product> {
  constructor () {
    super('/product')
  }
}

const ProductRepository = new ProductRepositoryClass()

export default ProductRepository
