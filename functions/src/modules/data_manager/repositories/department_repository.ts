import Repository from '../core/repository'
import Department from '../models/department.model'

class DepartmentRepositoryClass extends Repository<Department> {
  constructor () {
    super('/department')
  }
}

const DepartmentRepository = new DepartmentRepositoryClass()

export default DepartmentRepository
