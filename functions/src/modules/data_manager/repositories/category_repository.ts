import Repository from '../core/repository'
import Category from '../models/category.model'

class CategoryRepositoryClass extends Repository<Category> {
  constructor () {
    super('/category')
  }
}

const CategoryRepository = new CategoryRepositoryClass()

export default CategoryRepository
