import database from '../../../database'

export default abstract class Repository<T> {
  private model: string;

  constructor (model: string) {
    this.model = model
  }

  private generateRef = (id: string = '') => {
    return `${this.model}${id ? `/${id}` : ''}`
  }

  public getAll = (): Promise<T[]> => {
    const model = database.ref(this.generateRef())
    const items: T[] = []
    return new Promise((resolve, reject) => {
      model.on('value', (snapshot: any) => {
        snapshot.forEach((item: any) => {
          items.push({
            ...item.val(),
            id: item.key
          })
        })
        resolve(items)
      }, (error: any) => {
        reject(error)
      })
    })
  }

  public get = (id: string): Promise<T> => {
    const model = database.ref(this.generateRef(id))
    return new Promise((resolve, reject) => {
      model.on('value', (snapshot: any) => {
        resolve({ id: snapshot.key, ...snapshot.val() })
      }, (error: any) => {
        reject(error)
      })
    })
  }

  public add = (data: T): Promise<T> => {
    const model = database.ref(this.generateRef())

    model.push(data)
    return new Promise((resolve, reject) => {
      model.on('child_added', (snapshot: any) => {
        resolve({ id: snapshot.key, ...snapshot.val() })
      }, (error: any) => {
        reject(error)
      })
    })
  }

  public update = (id: string, data: T): Promise<T> => {
    const updates: any = {}

    updates[this.generateRef(id)] = data

    return new Promise((resolve, reject) => {
      database.ref().update(updates).then(() => {
        const model = database.ref(this.generateRef(id))
        model.on('value', (snapshot: any) => {
          resolve({ id: snapshot.key, ...snapshot.val() })
        }, (error: any) => {
          reject(error)
        })
      }).catch(error => {
        reject(error)
      })
    })
  }

  public delete = async (ids: string[]): Promise<Record<string, boolean>> => {
    const deleted: Record<string, boolean> = {}
    for (const id of ids) {
      try {
        await database.ref(this.generateRef(id)).remove()
        deleted[id] = true
      } catch (error) {
        deleted[id] = false
      }
    }
    return Promise.resolve(deleted)
  }
}
