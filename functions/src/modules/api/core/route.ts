import * as functions from 'firebase-functions'
import cors from '../../../cors'

export type RequestHandler = (req: functions.https.Request, res: functions.Response) => Promise<void>

const globalMethod = (method: string, handler: RequestHandler) => {
  return functions.https.onRequest((req, res) => {
    return cors(req, res, () => {
      if (req.method !== method) {
        return res.status(401).json({
          message: 'Not allowed'
        })
      }

      try {
        return handler(req, res)
      } catch (error) {
        return res.status(error.code || 500).json({
          message: `Something went wrong. ${error.message}`
        })
      }
    })
  })
}

const get = (handler: RequestHandler) => {
  return globalMethod('GET', handler)
}

const post = (handler: RequestHandler) => {
  return globalMethod('POST', handler)
}

const put = (handler: RequestHandler) => {
  return globalMethod('PUT', handler)
}

const remove = (handler: RequestHandler) => {
  return globalMethod('DELETE', handler)
}

export default {
  get,
  post,
  put,
  delete: remove
}
