import route from '../core/route'
import Products from '../../use_cases/products'

const getAllProducts = route.get(async (_, res) => {
  const products = await Products.getAllProducts()
  res.status(200).json(products)
})

const getProduct = route.get(async (req, res) => {
  const id = req.query.id as any
  const product = await Products.getProduct({ id })
  res.status(200).json(product)
})

const addProduct = route.post(async (req, res) => {
  const { name, departmentRef, cost, categoryRef } = req.body
  const product = await Products.addProduct({ name, departmentRef, cost, categoryRef })
  res.status(200).json(product)
})

const updateProduct = route.put(async (req, res) => {
  const { name, departmentRef, cost, categoryRef } = req.body
  const id = req.query.id as any
  const product = await Products.updateProduct({ id, data: { name, departmentRef, cost, categoryRef } })
  res.status(200).json(product)
})

const deleteProducts = route.delete(async (req, res) => {
  const ids = (req.query.ids as any).split(',')
  const deletedProducts = await Products.deleteProducts({ ids })
  res.status(200).json(deletedProducts)
})

export {
  getAllProducts,
  addProduct,
  getProduct,
  updateProduct,
  deleteProducts
}
