import route from '../core/route'
import Departments from '../../use_cases/departments'

const getAllDepartments = route.get(async (_, res) => {
  const departments = await Departments.getAllDepartments()
  res.status(200).json(departments)
})

const getDepartment = route.get(async (req, res) => {
  const id = req.query.id as any
  const department = await Departments.getDepartment({ id })
  res.status(200).json(department)
})

const addDepartment = route.post(async (req, res) => {
  const { name } = req.body
  const department = await Departments.addDepartment({ name })
  res.status(200).json(department)
})

const updateDepartment = route.put(async (req, res) => {
  const { name } = req.body
  const id = req.query.id as any
  const department = await Departments.updateDepartment({ id, data: { name } })
  res.status(200).json(department)
})

const deleteDepartments = route.delete(async (req, res) => {
  const ids = (req.query.ids as any).split(',')
  const deletedDepartments = await Departments.deleteDepartments({ ids })
  res.status(200).json(deletedDepartments)
})

export {
  getAllDepartments,
  addDepartment,
  getDepartment,
  updateDepartment,
  deleteDepartments
}
