import route from '../core/route'
import Categories from '../../use_cases/categories'

const getAllCategories = route.get(async (_, res) => {
  const categoriess = await Categories.getAllCategories()
  res.status(200).json(categoriess)
})

const getCategory = route.get(async (req, res) => {
  const id = req.query.id as any
  const category = await Categories.getCategory({ id })
  res.status(200).json(category)
})

const addCategory = route.post(async (req, res) => {
  const { name, departmentRef } = req.body
  const category = await Categories.addCategory({ name, departmentRef })
  res.status(200).json(category)
})

const updateCategory = route.put(async (req, res) => {
  const { name, departmentRef } = req.body
  const id = req.query.id as any
  const category = await Categories.updateCategory({ id, data: { name, departmentRef } })
  res.status(200).json(category)
})

const deleteCategories = route.delete(async (req, res) => {
  const ids = (req.query.ids as any).split(',')
  const deletedCategories = await Categories.deleteCategories({ ids })
  res.status(200).json(deletedCategories)
})

export {
  getAllCategories,
  addCategory,
  getCategory,
  updateCategory,
  deleteCategories
}
