import CategoryRepository from '../../data_manager/repositories/category_repository'
import Category from '../../data_manager/models/category.model'

export type GetCategoryParams = Category;

const addCategory = ({ name, departmentRef }: GetCategoryParams) => {
  return CategoryRepository.add({ name, departmentRef })
}

export default addCategory
