import getAllCategories from './get_all'
import addCategory from './add'
import getCategory from './get'
import updateCategory from './update'
import deleteCategories from './delete'

const Categories = {
  getAllCategories,
  addCategory,
  getCategory,
  updateCategory,
  deleteCategories
}

export default Categories
