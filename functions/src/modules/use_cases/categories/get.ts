import CategoryRepository from '../../data_manager/repositories/category_repository'

export interface GetCategoryParams {
  id: string
}

const getCategory = ({ id }: GetCategoryParams) => {
  return CategoryRepository.get(id)
}

export default getCategory
