import CategoryRepository from '../../data_manager/repositories/category_repository'

const getAllCategories = () => {
  return CategoryRepository.getAll()
}

export default getAllCategories
