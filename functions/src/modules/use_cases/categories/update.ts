import CategoryRepository from '../../data_manager/repositories/category_repository'
import Category from '../../data_manager/models/category.model'

export interface UpdateCategoryParams {
  id: string;
  data: Category;
}

const updateCategory = ({ id, data }: UpdateCategoryParams) => {
  return CategoryRepository.update(id, data)
}

export default updateCategory
