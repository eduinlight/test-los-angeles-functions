import CategoryRepository from '../../data_manager/repositories/category_repository'

export interface DeleteCategorysParams {
  ids: string[];
}

const deleteCategories = ({ ids }: DeleteCategorysParams) => {
  return CategoryRepository.delete(ids)
}

export default deleteCategories
