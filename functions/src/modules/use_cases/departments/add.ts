import DepartmentRepository from '../../data_manager/repositories/department_repository'
import Department from '../../data_manager/models/department.model'

export type GetDepartmentParams = Department;

const addDepartment = ({ name }: GetDepartmentParams) => {
  return DepartmentRepository.add({ name })
}

export default addDepartment
