import DepartmentRepository from '../../data_manager/repositories/department_repository'

const getAllDepartments = () => {
  return DepartmentRepository.getAll()
}

export default getAllDepartments
