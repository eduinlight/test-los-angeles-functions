import DepartmentRepository from '../../data_manager/repositories/department_repository'

export interface DeleteDepartmentsParams {
  ids: string[];
}

const deleteDepartments = ({ ids }: DeleteDepartmentsParams) => {
  return DepartmentRepository.delete(ids)
}

export default deleteDepartments
