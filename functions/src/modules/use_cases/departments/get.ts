import DepartmentRepository from '../../data_manager/repositories/department_repository'

export interface GetDepartmentParams {
  id: string
}

const getDepartment = ({ id }: GetDepartmentParams) => {
  return DepartmentRepository.get(id)
}

export default getDepartment
