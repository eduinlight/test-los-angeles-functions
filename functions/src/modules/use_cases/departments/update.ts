import DepartmentRepository from '../../data_manager/repositories/department_repository'
import Department from '../../data_manager/models/department.model'

export interface UpdateDepartmentParams {
  id: string;
  data: Department;
}

const updateDepartment = ({ id, data }: UpdateDepartmentParams) => {
  return DepartmentRepository.update(id, data)
}

export default updateDepartment
