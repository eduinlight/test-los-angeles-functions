import getAllDepartments from './get_all'
import addDepartment from './add'
import getDepartment from './get'
import updateDepartment from './update'
import deleteDepartments from './delete'

const Departments = {
  getAllDepartments,
  addDepartment,
  getDepartment,
  updateDepartment,
  deleteDepartments
}

export default Departments
