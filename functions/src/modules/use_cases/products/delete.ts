import ProductRepository from '../../data_manager/repositories/product_repository'

export interface DeleteProductsParams {
  ids: string[];
}

const deleteProducts = ({ ids }: DeleteProductsParams) => {
  return ProductRepository.delete(ids)
}

export default deleteProducts
