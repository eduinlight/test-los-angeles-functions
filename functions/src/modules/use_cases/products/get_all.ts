import ProductRepository from '../../data_manager/repositories/product_repository'

const getAllProducts = () => {
  return ProductRepository.getAll()
}

export default getAllProducts
