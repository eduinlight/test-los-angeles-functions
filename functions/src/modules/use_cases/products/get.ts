import ProductRepository from '../../data_manager/repositories/product_repository'

export interface GetProductParams {
  id: string
}

const getProduct = ({ id }: GetProductParams) => {
  return ProductRepository.get(id)
}

export default getProduct
