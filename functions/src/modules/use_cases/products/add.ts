import ProductRepository from '../../data_manager/repositories/product_repository'
import Product from '../../data_manager/models/product.model'

export type GetProductParams = Product;

const addProduct = ({ name, cost, departmentRef, categoryRef }: GetProductParams) => {
  return ProductRepository.add({ name, cost, departmentRef, categoryRef })
}

export default addProduct
