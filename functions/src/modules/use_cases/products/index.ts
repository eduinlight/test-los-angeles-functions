import getAllProducts from './get_all'
import addProduct from './add'
import getProduct from './get'
import updateProduct from './update'
import deleteProducts from './delete'

const Categories = {
  getAllProducts,
  addProduct,
  getProduct,
  updateProduct,
  deleteProducts
}

export default Categories
