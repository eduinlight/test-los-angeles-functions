import ProductRepository from '../../data_manager/repositories/product_repository'
import Product from '../../data_manager/models/product.model'

export interface UpdateProductParams {
  id: string;
  data: Product;
}

const updateProduct = ({ id, data }: UpdateProductParams) => {
  return ProductRepository.update(id, data)
}

export default updateProduct
