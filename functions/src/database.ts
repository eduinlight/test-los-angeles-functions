import * as admin from 'firebase-admin'

const database = admin.database()

export default database
