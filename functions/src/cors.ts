import * as corsOrigin from 'cors'

const cors = corsOrigin({ origin: true })

export default cors
