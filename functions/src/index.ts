import './initialize'
import { getAllDepartments, deleteDepartments, addDepartment, getDepartment, updateDepartment } from './modules/api/controllers/departments'
import { getAllCategories, deleteCategories, addCategory, getCategory, updateCategory } from './modules/api/controllers/categories'
import { getAllProducts, deleteProducts, addProduct, getProduct, updateProduct } from './modules/api/controllers/products'

export {
  // departments
  getAllDepartments,
  getDepartment,
  addDepartment,
  updateDepartment,
  deleteDepartments,
  // categories,
  getAllCategories,
  getCategory,
  addCategory,
  updateCategory,
  deleteCategories,
  // products
  getAllProducts,
  getProduct,
  updateProduct,
  addProduct,
  deleteProducts
}
